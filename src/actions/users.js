import axios from 'axios';
import config from '../config';

const path = config.path_api;

//Usuarios del Sistema

  export function getSpecificSysUser(id){
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('dte_token');
    return axios.get(`${path}/systemuser/` + id);
  }
  
  export function createSysUser(first, last, username, password, email, type) {
  axios.defaults.headers.common['Authorization'] = localStorage.getItem('dte_token');
  /*if(first !== null && first !== ''){data.append('first', first);}
  if(last !== null && last !== ''){data.append('last', last);}
  if(email !== null && email !== ''){data.append('email', email);}*/
    return axios.post(`${path}/systemuser/`, { first: first, last: last, username: username, password:password, email: email , id_systemUserType: type});
  }

  export function getAllSysUser(){
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('dte_token');
    return axios.get(`${path}/systemuser/`);
  }
  
  export function editSysUser(id, first, last, username, email) {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('dte_token');
    let data = new FormData();
  if(first !== null && first !== ''){data.append('first', first);}
  if(last !== null && last !== ''){data.append('last', last);}
  if(email !== null && email !== ''){data.append('email', email);}
  console.log(email)
  data.append('username',username);  
    return axios.put(`${path}/systemuser/` + id, { first: first, last: last, username: username, email: email });
  }
  
  export function deleteSysUser(id){
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('dte_token');
    return axios.delete(`${path}/systemuser/` + id);
  }

  // Usuarios de Aplicaciones

  export function getAllAccount(page){
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('dte_token');
    return axios.get(`${path}/accountusr/page/` + page);
  }
  