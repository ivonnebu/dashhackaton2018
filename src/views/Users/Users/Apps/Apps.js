import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import {getAllAccount} from "../../../actions/user"
class Apps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postId: '',
      loading: false,
      users: [],
      currentPage: 1,
      itemsPerPage: 10,
      pages: 1,
      modalMessage: false,
      colorAlert: 'success ',
      boxAlert: false,
      msgAlert: ''
    }
    this.handleClickPage =  this.handleClickPage.bind(this);
  }

  componentDidMount(){
    this.getAllUsers(this.state.currentPage);
  }
  
  handleClickPage = event => {
    let number = Number(event.target.id)
    this.setState({ currentPage: number });
    this.getAllUsers(number);
  };
  getAllUsers = (page) => {
    getAllAccount(page)
    .then(res => {
      console.log(res.data.accountUsers)
      this.setState({users: res.data.accountUsers, pages: res.data.pages, itemsPerPage: res.data.accountUsers.count})
    })
  }
  render () {
    const { users, currentPage, itemsPerPage, pages } = this.state;

    // Show items
    const indexOfLastItems = currentPage * itemsPerPage;
    const indexOfFirstItems =  indexOfLastItems - itemsPerPage;
    const currentItems =  users;

    const renderAlls = currentItems.map((item,index) => {
      console.log(item);
      return <tr key={ index.toString() }>
        <td>{ item.name }</td>
        <td>{ item.User.email }</td>
        <td>{ item.socialNumber }</td>
        <td>{ item.PersonType.description }</td>
        <td>{ item.status } </td>
      </tr>;
    });

    //Logic Numbers
    const pageNumbers = [];
    for(let i = 1; i <= pages; i++){
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <PaginationItem key={ number.toString() } >
          <PaginationLink tag="button" id={number} onClick={this.handleClickPage}>
            {number}
          </PaginationLink>
        </PaginationItem>
      );
    });

    return (
      <div className="animated-fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-user-circle"></i><strong>Usuarios del aplicaciones</strong>
              </CardHeader>
              <CardBody>
                <Table hover bordered striped responsive className="text-center">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Email</th>
                      <th>Número Social</th>
                      <th>Tipo</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  { renderAlls }
                  </tbody>
                </Table>
                <nav>
                  <Pagination>
                    { renderPageNumbers }
                  </Pagination>
                </nav>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Apps;
