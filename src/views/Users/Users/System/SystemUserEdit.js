import React, { Component } from 'react';
import { Alert, Button, Card, CardBody, CardHeader, Col, Form, FormGroup, InputGroup, InputGroupAddon, Input, InputGroupText, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import queryString from "query-string";
//Actions
import { getSpecificSysUser, editSysUser } from "../../../actions/user"

class SystemUserEdit extends Component {
    constructor(props){
        super(props);
        this.state = {
          id: '',
          name:'',
          first: '',
          last : '',
          email: '',
          username:'',
          role: '',
          url: '',
          loading: false,
          modalMessage: false,
          colorAlert: 'success ',
          boxAlert: false,
          msgAlert: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormUserSubmit = this.handleFormUserSubmit.bind(this);
      }

      componentDidMount(){
        let parseParam = queryString.parse(this.props.location.search);
        console.log("ID DEL USUARIO " + parseParam.id);
        getSpecificSysUser(parseParam.id)
          .then(res => {
            console.log(res.data);
            let dataUser = res.data.systemUser;
            console.log(dataUser);
            this.setState({
              id: dataUser.id,
              username: dataUser.username,
              first: (dataUser.first === null) ? '' : dataUser.first,
              last: (dataUser.last === null) ? '' : dataUser.last,
              email: (dataUser.email === null) ? '' : dataUser.email
            });
          })
          .catch(err => {
            console.log(err.response);
            this.setState({colorAlert: 'danger'});
            this.showAlert("Error al obtener datos.");
          })
      }

      handleInputChange = event => {
        this.setState({
          [event.target.name]: event.target.value
        });
        event.preventDefault();
      };
    
      handleFileImageChange = event => {
        let extension = event.target.files[0].type;
        let size = event.target.files[0].size;
        console.log(event.target.files[0]);
        if(size <= 1048576){
          if(extension === 'image/jpeg' || extension === 'image/png'){
            this.setState({
              [event.target.name]: event.target.value
            });
            let reader = new FileReader();
            let file = event.target.files[0];
    
            reader.onloadend = () => {
              this.setState({
                image: file,
                imagePreviewUrl: reader.result
              });
            };
            reader.readAsDataURL(file);
          }
          else {
            this.setState({colorAlert: 'danger'});
            this.showAlert("Formato de imagen no soportado.");
          }
        }
        else {
          this.setState({colorAlert: 'danger'});
          this.showAlert("Imagen excede el peso permitido.");
        }
        event.preventDefault();
      };
    
      handleFormUserSubmit = event => {
        event.preventDefault();
        let { id, first, last,email,username} = this.state;
        console.log(email)
        if( username !== undefined ){
          editSysUser(id, first, last, username, email)
            .then(res => {
              console.log(res.data);
              if(res.data.success){
                this.setState({colorAlert: 'success'});
                //this.resetFormPost();
                this.showAlert("Datos almacenados.");
              }
            })
            .catch(err => {
              console.log(err.response);
              if(err.response.status === 500 || err.response.status === 422){
                this.setState({colorAlert: 'danger'});
                this.showAlert("Error interno de servidor");
              }
            })
        } else {
          this.setState({colorAlert: 'danger'});
          this.showAlert("Faltan datos por ingresar.");
        }
        console.log("Fin Envio");
      };
    
      resetFormPost = () => {
        this.setState({
          description: '',
          imageName: '',
          image: null,
          url: '',
          imagePreviewUrl: ''
        })
      };
    
      validateForm(){
        return this.state.description.length > 0 && this.state.description.length > 0;
      };
    
      showAlert = dataAlert => {
        this.setState({
          boxAlert: true,
          msgAlert: dataAlert
        });
      };
    
      onDismiss = () =>{
        this.setState({ boxAlert:false });
      };

      render(){
        return(
            <div className="animated-fadeIn">
            <Row className="justify-content-center">
              <Col md="8">
                <Card className="m-4">
                  <CardBody className="p-5">
                    <h3 className="mb-3">Editar Usuario del Sistema</h3>
                    <Form className="row" onSubmit={ this.handleFormUserSubmit }>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="first" placeholder="Nombre" 
                        value={ this.state.first }
                        onChange={ this.handleInputChange } />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="last" placeholder="Apellido" 
                        value={ this.state.last }
                        onChange={ this.handleInputChange }/>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                      <Input type="text" name="email" placeholder="Email" 
                        value={ this.state.email }
                        onChange={ this.handleInputChange }/>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-user-circle"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" name="username" placeholder="Usuario" 
                        value={ this.state.username }
                        onChange={ this.handleInputChange }/>
                    </InputGroup>
                    <Button color="success" block type="submit">Actualizar Usuario</Button>
                    <Alert className="mt-2" color={ this.state.colorAlert }
                               isOpen={ this.state.boxAlert }
                               toggle={ this.onDismiss }>
                          { this.state.msgAlert }
                        </Alert>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        );
      }
}
export default SystemUserEdit