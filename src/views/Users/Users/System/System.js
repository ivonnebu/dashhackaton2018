import React, { Component } from 'react';
import { Alert, Button, Card, CardBody, CardHeader, Col, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import {getAllSysUser, deleteSysUser} from "../../../actions/user"

class System extends Component {
  constructor(props){
    super(props);
    this.state = {
      userId: '',
      loading: false,
      admins: [],
      currentPage: 1,
      itemsPerPage: 10,
      modalMessage: false,
      colorAlert: 'success ',
      boxAlert: false,
      msgAlert: ''
    }

    //this.handleClick = this.handleClick.bind(this);
  }
  componentDidMount(){
    this.getListUsers();
  }
  getListUsers = () =>{
    getAllSysUser()
    .then( res => {
      //console.log(res.data)
      this.setState({admins: res.data.systemUsers})
    })
    .catch(err => {
      console.log(err.response);
        if(err.response.status === 500 || err.response.status === 422){
          this.setState({colorAlert: 'danger'});
          this.showAlert(err.response.data.error);
        }
    })
  }
  redirectFormUser = event => {
    event.preventDefault();
    this.props.history.push('/users/register');
  };
  handleModalUser = event => {
    let userId = event.target.attributes.getNamedItem('data-id').value;
    this.setState({
      userId: userId
    });
    this.toggleModal();
  };
  toggleModal = event => {
    this.setState({
      modalMessage: !this.state.modalMessage
    })
  };
  handleEditUser = event => {
    let userId = event.target.attributes.getNamedItem('data-id').value;
    this.props.history.push('/users/edit?id=' + userId);
  };
  handleDeleteUser= event => {
    event.preventDefault();
    console.log(this.state.userId);
    deleteSysUser(this.state.userId)
      .then(res => {
        console.log(res.data);
        this.setState({colorAlert: 'success'});
        this.showAlert("Item eliminado.");
        this.getListUsers();
      })
      .catch(err => {
        console.log(err.response);
        this.setState({colorAlert: 'danger'});
        this.showAlert("Error al eliminar item.");
      });
    this.toggleModal();
  };
  showAlert = dataAlert => {
    this.setState({
      boxAlert: true,
      msgAlert: dataAlert
    });
  };

  onDismiss = () =>{
    this.setState({ boxAlert:false });
  };
  render() {
    const { admins, currentPage, itemsPerPage} = this.state;

    // Show items
    const indexOfLastItems = currentPage * itemsPerPage;
    const indexOfFirstItems =  indexOfLastItems - itemsPerPage;
    const currentItems =  admins;
    const renderAlls = currentItems.map((item,index) => {
      console.log(item)
      return <tr key={ index.toString() }>
        <td>{ (item.first === null) ? "": item.first}  { (item.last === null) ? "": item.last}</td>
        <td>{ item.username}</td>
        <td>{ (item.email === null) ? "": item.email}</td>
        <td>{item.SystemUserType.description}</td>
        <td>
          <Button type="button" data-id={ item.id } className="btn mx-2" onClick={ this.handleEditUser }>
            <i className="fa fa-pencil"></i>&nbsp;Editar
          </Button>
          <Button type="button" data-id={ item.id } className="btn mx-2" onClick={ this.handleModalUser }>
            <i className="fa fa-trash"></i>&nbsp;Eliminar
          </Button>
        </td>
      </tr>;
    });

    //Logic Numbers
    const pageNumbers = [];
    for(let i = 1; i <= Math.ceil(admins.length / itemsPerPage); i++){
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <PaginationItem key={ number }>
          <PaginationLink tag="button" id={number} onClick={this.handleClick} >
            {number}
          </PaginationLink>
        </PaginationItem>
      );
    });

    return (
      <div className="animated-fadeIn">
      <Modal isOpen={ this.state.modalMessage } >
          <ModalHeader>Advertencia!</ModalHeader>
          <ModalBody>¿Esta seguro de eliminar este item?</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={ this.handleDeleteUser }>Eliminar</Button>
            <Button color="secondary" onClick={ this.toggleModal }>Cancel</Button>
          </ModalFooter>
        </Modal>
        <Row>
          <Col>
          <Alert className="mt-2" color={ this.state.colorAlert }
                   isOpen={ this.state.boxAlert }
                   toggle={ this.onDismiss }>
              { this.state.msgAlert }
            </Alert>
            <Card>
              <CardHeader>
                <i className="fa fa-user-circle"></i><strong>Usuarios del Sistema</strong>
                <div className="card-header-actions">
                  <Button color="primary" onClick={this.redirectFormUser}>Añadir Usuario </Button>
                </div>
              </CardHeader>
              <CardBody>
                <Table hover bordered striped responsive className="text-center" >
                  <thead>
                    <tr>
                      <th>Nombre Completo</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Rol</th>
                      <th className="w-25">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    { renderAlls }
                  </tbody>
                </Table>
                <nav>
                  <Pagination>
                    {/*<PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>*/}
                    { renderPageNumbers }
                    {/*<PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>*/}
                  </Pagination>
                </nav>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default System;
