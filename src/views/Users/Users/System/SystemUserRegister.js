import React, { Component } from 'react';
import { Alert, Button, Card, CardBody, CardHeader, Col, Form, FormGroup, Input, Label, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {createSysUser} from "../../../actions/user"


class SystemUserRegister extends Component{
  constructor(props){
    super(props);
    this.state = {
      id: '',
      name:'',
      first: '',
      last : '',
      email: '',
      username:'',
      type: 1,
      password: '',
      loading: false,
      admins: [],
      currentPage: 1,
      itemsPerPage: 10,
      modalMessage: false,
      colorAlert: 'success ',
      boxAlert: false,
      msgAlert: ''
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFormUserSubmit = this.handleFormUserSubmit.bind(this);
  }

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
    event.preventDefault();
  };

  handleFormUserSubmit = event => {
    event.preventDefault();
    let {first, last, username, password, email, type } = this.state;
    if( username !== undefined && password !== undefined){
      console.log(first, last, username, password, email, type)
      createSysUser(first, last, username, password, email, type )
        .then(res => {
          console.log(res.data);
          if(res.data.success){
            this.setState({colorAlert: 'success'});
            //this.resetFormPost();
            this.showAlert("Datos almacenados.");
          }
        })
        .catch(err => {
          console.log(err.response);
          if(err.response.status === 500 || err.response.status === 422){
            this.setState({colorAlert: 'danger'});
            this.showAlert("Error interno de servidor");
          }
        })
    } else {
      this.setState({colorAlert: 'danger'});
      this.showAlert("Faltan datos por ingresar.");
    }
    console.log("Fin Envio");
  };
  showAlert = dataAlert => {
    this.setState({
      boxAlert: true,
      msgAlert: dataAlert
    });
  };
  validateForm(){
    return this.state.username.length > 0 && this.state.password.length > 0;
  };
  onDismiss = () =>{
    this.setState({ boxAlert:false });
  };


  render() {
    return (
      <div className="animated-fadeIn">
        <Row className="justify-content-center">
          <Col md="8">
            <Card className="m-4">
              <CardBody className="p-5">
                <h3 className="mb-3">Crear Usuario del Sistema</h3>
                <Form className="row" onSubmit={ this.handleFormUserSubmit }>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-user"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" name="first" placeholder="Nombre" 
                   value={ this.state.first }
                   onChange={ this.handleInputChange } />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-user"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" name="last" placeholder="Apellido" 
                  value={ this.state.last }
                  onChange={ this.handleInputChange }/>
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                  <Input type="text" name="email" placeholder="Email" 
                  value={ this.state.email }
                  onChange={ this.handleInputChange }/>
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-user-circle"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text" name="username" placeholder="Usuario" 
                  value={ this.state.username }
                  onChange={ this.handleInputChange }/>
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-lock"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="password" name="password" placeholder="Contraseña" 
                  value={ this.state.password }
                  onChange={ this.handleInputChange }/>
                </InputGroup>
                <Button color="primary" type="submit" disabled={ !this.validateForm() }>Crear Usuario</Button>
                </Form>
                <Alert className="mt-2" color={ this.state.colorAlert }
                               isOpen={ this.state.boxAlert }
                               toggle={ this.onDismiss }>
                          { this.state.msgAlert }
                        </Alert>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SystemUserRegister;
