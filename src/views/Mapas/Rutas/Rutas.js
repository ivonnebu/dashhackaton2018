import React, { Component } from 'react';
import { Bar, Doughnut, Line, Pie, Polar, Radar } from 'react-chartjs-2';
import { Card, CardBody, CardColumns, CardHeader } from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';

import {Map, InfoWindow, Marker, GoogleApiWrapper, Polyline} from 'google-maps-react';

class MapContainer extends Component {
    render() {
        const triangleCoords = [
          {lat: 25.774, lng: -80.190},
          {lat: 18.466, lng: -66.118},
          {lat: 32.321, lng: -64.757},
          {lat: 25.774, lng: -80.190}
        ];
      
        return(
          <Map google={this.props.google}
              style={{width: '80%', height: '90%', position: 'absolute'}}
              className={'map'}
              zoom={14}>
              <Polyline
                paths={triangleCoords}
                strokeColor="#0000FF"
                strokeOpacity={0.8}
                strokeWeight={2} />
          </Map>
        )
      }      
}

const rutas = GoogleApiWrapper({
  apiKey: ('AIzaSyDzb2-dA_dBgUt3O8UjG_mCcP_O7FZ4DvU')
})(MapContainer)


export default rutas;